import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { counter } from "./reducers/counter.reducer";
import { PlaylistsState, playlists } from "./reducers/playlists.reducer";
import { SearchState, search } from "./reducers/search.reducer";
import { routerMiddleware, connectRouter } from "connected-react-router";

export type AppState = {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
};

const reducer = combineReducers<AppState>({
  counter,
  playlists,
  search
});

// ==

import { createBrowserHistory } from "history";

export const history = createBrowserHistory();

// history.push({
//   pathname:'/'
// })

export const store = createStore(
  connectRouter(history)(reducer),
  compose(
    applyMiddleware(routerMiddleware(history)),
    window["__REDUX_DEVTOOLS_EXTENSION__"] &&
      window["__REDUX_DEVTOOLS_EXTENSION__"]()
  )
);

window["store"] = store;

// store.subscribe(() => {
//   console.log("NEW State:", store.getState());
// });
