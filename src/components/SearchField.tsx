import * as React from "react";

type Props = {
  onSearch(query: string): void;
};

export class SearchField extends React.Component<Props> {
  debounceTimer: NodeJS.Timer;

  search = (event: React.ChangeEvent<HTMLInputElement>) => {
    const query = event.target.value;
    
    // Abort previous search
    clearTimeout(this.debounceTimer);

    // Delay search
    this.debounceTimer = setTimeout(() => {
      this.props.onSearch(query);
    }, 400);
  };

  render() {
    return (
      <div className="input-group mb-2">
        <input type="search" className="form-control" onChange={this.search} />
      </div>
    );
  }
}
