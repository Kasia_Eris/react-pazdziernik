import { Reducer, Action } from "redux";

type State = number;

const initialState: State = 0;

export const counter: Reducer<State, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "INCREMENT":
      return (state += action.payload);
    case "DECREMENT":
      return (state -= action.payload);
    default:
      return state;
  }
};

interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}
interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}

type Actions = INCREMENT | DECREMENT;
